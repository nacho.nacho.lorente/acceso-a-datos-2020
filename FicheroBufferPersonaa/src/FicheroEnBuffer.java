import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FicheroEnBuffer {
	private String nombreFichero;
	
	public FicheroEnBuffer(String nombre) {
		nombreFichero = nombre;
	}
	
	
	
	public void EscribirTabla(Persona[] tabla)
	{
		try {
			File fichero = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int i=0; i<tabla.length;i++) 
			{
				bw.write(tabla[i].getNombre()); // Escribe un nombre por linea
				bw.newLine();
				bw.write(Integer.toString(tabla[i].getEdad()));
				bw.newLine();
				bw.write(Integer.toString(tabla[i].getTelefono()));
				bw.newLine(); 
				bw.write(tabla[i].getDNI());
				bw.newLine(); 
			}
			bw.close();
			
		} catch (IOException e) {
			
		System.out.println(e.getMessage());
		}
	}
	
	public void LeerTabla(Persona[] tabla)
	{
		try {
			File fichero = new File(nombreFichero);
			FileReader ficheroFR = new FileReader(fichero); // Esto crea el fichero
			BufferedReader ficheroBR = new BufferedReader(ficheroFR);
			String linea;
			String nombreP,dniP;
			int edad;
			int telefono;
			
			// Leer en fichero
			linea = ficheroBR.readLine();// Se llama lectura avanzada primero lee y luego se incrementa, de esta manera nunca mete null
			while(linea != null)
			 {
				//Leer datos persona				
				nombreP = linea; 
				linea=ficheroBR.readLine();
				edad=Integer.parseInt(linea);
				linea = ficheroBR.readLine();
				telefono=Integer.parseInt(linea);
				linea = ficheroBR.readLine();
				dniP = linea;
				
				Persona p = new Persona(nombreP,edad, telefono, dniP);
				//insertarPersona(p);
				
				linea = ficheroBR.readLine();
			}
			
				ficheroBR.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

	}
}
