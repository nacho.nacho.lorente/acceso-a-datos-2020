
public class Persona {
	private String nombre;
	int edad;
	int telefono;
	String DNI;
	
	public Persona() {
		nombre = "";
		edad = 0;
		telefono = 0;
		DNI = "";
	}
	
	public Persona(String n, int e, int t, String dni) {
		nombre = n;
		edad = e;
		telefono = e;
		DNI = dni;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getEdad() {
		return edad;
	}
	
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	public int getTelefono() {
		return telefono;
	}
	
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
	
	public String getDNI() {
		return DNI;
	}
	
	public void setDNI(String DNI) {
		this.DNI = DNI;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", telefono=" + telefono + ", DNI=" + DNI + "]";
	}
	
}
