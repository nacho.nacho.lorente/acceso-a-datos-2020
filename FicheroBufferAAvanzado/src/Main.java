import java.util.Scanner;

public class Main {
	
	public static int menu()
	{
	Scanner reader = new Scanner(System.in);
	int numero;
	System.out.println("MENU DE OPCIONES");
	System.out.println(" 1. Escribir en fichero");
	System.out.println(" 2. Leer Fichero");
	System.out.println(" 3. Imprimir Fichero");
	System.out.println(" 4. Borrar datos");
	System.out.println(" 5. Salir");
	numero = reader.nextInt();
	reader.nextLine();
	return numero;
	}
	
	public static void main(String[] args) {
		Scanner reader;
		TablaNombres miFichero = new TablaNombres(); 
		String[] nombres = {"Juan", "Laura", "Eduardo", "Ana"};
		
		int opcion;
		do {
			opcion = menu();
			switch(opcion) {
			case 1 :
				miFichero.escribirFichero("Nombres.txt");
			break;
			case 2: 
				miFichero.leerFichero("Nombres.txt");
			break;
			case 3:
				miFichero.mostrarTabla();
			break;
			case 4:
				miFichero.vaciarTabla();
			break;
			}
		} while(opcion != 5);
		
		

	}

}
