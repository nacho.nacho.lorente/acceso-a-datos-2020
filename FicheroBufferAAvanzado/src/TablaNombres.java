import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TablaNombres {

	String[] tablasNombres = {"Juan Luis", "Ana", "Laura", "Pedro"};
	private int posicion, tama�o;
	
	public TablaNombres(){
	String[] tablasNombres = new String [30];
	posicion = 0;
	tama�o = 30;
	}
	
	public  void escribirFichero(String miFichero) {

		// Crear Fichero
		try {
			
		File fichero = new File(miFichero);
		FileWriter fw = new FileWriter(fichero);//Esto crea el fichero
		BufferedWriter bw = new BufferedWriter(fw);
		
		//Escribir en fichero
		
		for(int i = 0; i < tablasNombres.length; i++) {
			bw.write(tablasNombres[i]);
			bw.newLine();
		}
		bw.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
	}
	
	public  void leerFichero(String miFichero) {

		// Crear Fichero
		try {
			File fichero = new File(miFichero);
			FileReader ficheroFR = new FileReader(fichero); // Esto crea el fichero
			BufferedReader ficheroBR = new BufferedReader(ficheroFR);
			String linea;
			
			// Leer en fichero
			linea = ficheroBR.readLine();// Se llama lectura avanzada primero lee y luego se incrementa, de esta manera nunca mete null
			while(linea != null)
			 {
				tablasNombres[posicion] = linea; 
				posicion++;
				linea = ficheroBR.readLine();
			}
			
				ficheroBR.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}

		
	}
	
	
	public  void mostrarTabla() {
		System.out.println("El contenido es: ");
		for(int cnt = 0; cnt<tablasNombres.length; cnt++) {
			System.out.println(tablasNombres[cnt] + "-");
		}
		
		
	}
	
	public  void vaciarTabla() {
		for(int cnt = 0; cnt<tablasNombres.length; cnt++) {
			tablasNombres[cnt] = "";
		}
	}
	
}
