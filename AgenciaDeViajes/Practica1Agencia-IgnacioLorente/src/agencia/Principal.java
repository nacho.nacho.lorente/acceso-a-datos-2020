package agencia;

import java.util.Scanner;

import agencia.FicheroItinerarios.*; /*Tuve que crear el paquete agencia porque si no, no podia
										escribir el enum de una clase a otra con el default package.*/
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Principal {

	static final Scanner entrada = new Scanner(System.in);
	static FicheroItinerarios miFichero = new FicheroItinerarios(30);
	static File fichero = new File("Itinerarios.txt");
	static File ficheroMenor = new File("DivMayor.txt");
	static File ficheroMayor = new File("DivMenor.txt");

	enum mayor_menor {
		MAaME, MEaMA
	};

	public static int escribirMenu() {

		System.out.println("                            Menu                           ");
		System.out.println("-----------------------------------------------------------");
		System.out.println("|1. Leer fichero y cargar contenido                       |");
		System.out.println("|2. Insertar itinerario                                   |");
		System.out.println("|3. Borrar itinerario                                     |");
		System.out.println("|4. Borrar todos los datos                                |");
		System.out.println("|5. Modificar itinerario                                  |");
		System.out.println("|6. Guardar fichero                                       |");
		System.out.println("|7. Separar fichero                                       |");
		System.out.println("|8. Mostrar contenido por consola                         |");
		System.out.println("|9. Mostrar itinerarios de mayor a menor                  |");
		System.out.println("|10. Mostrar itinerarios de menor a mayor                 |");
		System.out.println("|11. Mostrar destino mas repetido de todos los itinerarios|");
		System.out.println("|12. Salir                                                |");
		System.out.println("-----------------------------------------------------------");

		return entrada.nextInt();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int opcion = 0;
		do {
			try {

				opcion = escribirMenu();
				switch (opcion) {
				case 1:
					// Leer y cargar
					miFichero.abrir(fichero, Apertura.LEER);
					miFichero.leer();
					miFichero.cerrar(Apertura.LEER);
					break;
				case 2:
					// Insertar itinerario
					miFichero.abrir(fichero, Apertura.ESCRIBIR);
					miFichero.escribir(entrada);
					miFichero.cerrar(Apertura.ESCRIBIR);
					break;
				case 3:
					// borrar itinerario
					miFichero.abrir(fichero, Apertura.LEER);
					miFichero.borrar(entrada);
					miFichero.cerrar(Apertura.LEER);
					break;
				case 4:
					// borrar todos los datos
					miFichero.vaciar(fichero);
					break;
				case 5:
					// modificar itinerario
					miFichero.borrar(entrada);
					modificar(entrada);
					miFichero.cerrar(Apertura.LEER);
					break;
				case 6:
					// guardar fichero
					guardar();
					break;
				case 7:
					// separar fichero
					separar(entrada);
					break;
				case 8:
					// mostrar contenido por consola
					mostrar();
					break;
				case 9:
					// itinerario mayor a menor
					mayorymenor(mayor_menor.MAaME);
					break;
				case 10:
					// itinerario menor a mayor
					mayorymenor(mayor_menor.MEaMA);
					break;
				case 11:
					// Destino mas repetido
					masRepe();
					break;
				case 12:
					// Salir
					break;

				}
			} catch (IOException e) {
				System.out.println("Error en menu");
			}

		} while (opcion != 12);

	}



	public static int getMinimo() {
		int minimo = 1000;

		for (int i = 0; i < miFichero.posicion; i++)
			if (miFichero.tablaIti[i].getCantidad() < minimo)
				minimo = miFichero.tablaIti[i].getCantidad();

		return minimo;
	}

	public static int getMaximo() {
		int maximo = -1000;

		for (int i = 0; i < miFichero.posicion; i++)
			if (miFichero.tablaIti[i].getCantidad() > maximo)
				maximo = miFichero.tablaIti[i].getCantidad();

		return maximo;
	}

	public static void mayorymenor(mayor_menor tipo) {
		int maximo = getMaximo();
		String max = "";
		int minimo = getMinimo();
		String min = "";

		if (tipo == mayor_menor.MAaME) {
			for (int i = maximo; i >= minimo; i--) {// Compara el numero m�ximo con el m�nimo y saca decrementando desde
													// el maximo
				for (int cnt = 0; cnt < miFichero.posicion; cnt++) {
					if (miFichero.tablaIti[cnt].getCantidad() == i) {
						max = miFichero.tablaIti[cnt].toString();
						System.out.println(max);

					}

				}


			}

		}

		if (tipo == mayor_menor.MEaMA) {

			for (int i = minimo; i >= maximo; i++) {// Compara el minimo con el maximo y va incrementando hasta el
													// maximo
				for (int cnt = 0; cnt < miFichero.posicion; cnt++) {
					if (miFichero.tablaIti[cnt].getCantidad() == i) {
						min = miFichero.tablaIti[cnt].toString();
						System.out.println(min);

					}


				}

			}

		}

	}

	public static void guardar() {

		try {
			File fich = new File("Itinerarios.txt");
			FileWriter fw1 = new FileWriter(fich, true);
			BufferedWriter bw2 = new BufferedWriter(fw1);

			for (int i = 0; i < miFichero.posicion; i++) {
				if (miFichero.tablaIti[i] != null) {
					bw2.write(miFichero.tablaIti[i].toString() + "\n");
				}
			}
			bw2.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void mostrar() {
		for (int i = 0; i < miFichero.posicion; i++) {
			System.out.println(miFichero.tablaIti[i].toString());
		}
	}

	public static void modificar(Scanner sc) throws IOException {

		int cant_des;
		int iti;
		String[] ciu;

		for (int i = 0; i < miFichero.tablaIti.length; i++) {
			if (miFichero.tablaIti[i] != null) {
				System.out.println("[" + i + "]" + miFichero.tablaIti[i]);
			}
		}
		System.out.println("Indique Itinerario a modificar: ");
		iti = sc.nextInt();

		System.out.print("Introduzca la nueva cantidad de destinos: ");
		cant_des = sc.nextInt();

		miFichero.tablaIti[iti].setCantidad(cant_des);

		ciu = new String[cant_des];
		for (int i = 0; i < cant_des; i++) {
			System.out.print("Introduzca la ciudad numero " + (i + 1) + ":");
			ciu[i] = sc.next();
		}

		miFichero.tablaIti[iti].setCiudades(ciu);

	}

	public static void masRepe() {
		String repe = "";
		int mayor = -10000;
		int cnt = 0;

		// Realizo 2 bucles para recorrer el array y comparar uno a uno cual es el mas
		// repetido
		for (int i = 0; i < miFichero.tablaIti.length; i++) {
			for (int x = 0; x < miFichero.tablaIti.length; x++)
				if (miFichero.tablaIti[i] != null && miFichero.tablaIti[x] != null) {
					// Comparamos la posicion i con el resto del array en el bucle x y si coincide
					// se incrementa cnt
					if (miFichero.tablaIti[i].getCantidad() == miFichero.tablaIti[x].getCantidad()) {
						cnt++;
					}
					if (mayor < cnt) { // Si cnt es superior a mayor prevalece como el maximo de destino
						mayor = cnt;
						repe = miFichero.tablaIti[i].toString();
					}
				}
		}
		System.out.println("El destino mas repetido es :" + repe);
	}

	public static void separar(Scanner sc) throws IOException {
		int div;

		System.out.print("Escriba division: ");
		div = sc.nextInt();

		FileWriter fr1 = new FileWriter(ficheroMenor);
		BufferedWriter bw1 = new BufferedWriter(fr1);

		FileWriter fr2 = new FileWriter(ficheroMayor);
		BufferedWriter bw2 = new BufferedWriter(fr2);

		for (int i = 0; i < miFichero.tablaIti.length; i++) {
			if (miFichero.tablaIti[i] != null) {
				if (i >= div) {
					bw1.write(miFichero.tablaIti[i].toString() + "\n");
				} else {
					bw2.write(miFichero.tablaIti[i].toString() + "\n");
				}
			}
		}

		bw1.close();
		bw2.close();

	}

}
