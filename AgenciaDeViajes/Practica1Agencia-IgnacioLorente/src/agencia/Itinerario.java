package agencia;

import java.util.Arrays;

public class Itinerario {
	private String zona;
	private int cantidad;
	private String[] ciudades;

	public Itinerario(String zona, int cantidad, String[] ciudades) {
		this.zona = zona;
		this.cantidad = cantidad;
		this.ciudades = ciudades;
	}

	public Itinerario() {
		// TODO Auto-generated constructor stub
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String[] getCiudades() {
		return ciudades;
	}

	public void setCiudades(String[] ciudad) {
		this.ciudades = ciudad;
	}

	@Override
	public String toString() {
		return zona + "\n" + cantidad + "\n" + Arrays.toString(ciudades);
	}

}