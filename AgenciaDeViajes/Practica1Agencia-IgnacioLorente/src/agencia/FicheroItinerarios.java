package agencia;

import agencia.Itinerario;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FicheroItinerarios {

	public File fichero = new File("itinerarios.txt");
	public int posicion;
	public Itinerario[] tablaIti;

	enum Apertura {
		LEER, ESCRIBIR
	};

	Apertura tipoA;
	private FileReader fr;
	private BufferedReader br;
	private FileWriter fw;
	private BufferedWriter bw;
	// private Scanner entrada = new Scanner(System.in);

	public FicheroItinerarios(int longitud) {
		tablaIti = new Itinerario[longitud];
		posicion = 0;

	}

	public void abrir(File nombrefichero, Apertura tipo){
		try {
			if (nombrefichero.createNewFile()) {
				System.out.println("Fichero creado");
			}

			else {
				System.out.println("Fichero ya existente");
			}

			if (tipo == Apertura.LEER) {
				tipoA = Apertura.LEER;
				fr = new FileReader(nombrefichero);
				br = new BufferedReader(fr);

			}

			else if (tipo == Apertura.ESCRIBIR) {
				tipoA = Apertura.ESCRIBIR;
				fw = new FileWriter(nombrefichero);
				bw = new BufferedWriter(fw);

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error al crear fichero" + e.getMessage());
		}

	}

	public Itinerario[] escribir(Scanner sc) throws IOException {
		String zona;
		int cantidad;
		String[] ciudad;

		System.out.print("Nombre Itinerario: ");
		zona = sc.next();

		System.out.print("Cantidad de destinos: ");
		cantidad = sc.nextInt();

		ciudad = new String[cantidad];
		for (int i = 0; i < cantidad; i++) {
			System.out.print("Introduzca la ciudad numero " + (i + 1) + ":");
			ciudad[i] = sc.next();
		}

		Itinerario iti = new Itinerario(zona, cantidad, ciudad);

		tablaIti[posicion] = iti;
		posicion++;

		for (int i = 0; i < posicion; i++) {
			System.out.println(tablaIti[i].toString());
		}
		return tablaIti;

	}

	public void leer() {

		try {
			br = new BufferedReader(fr);

			String linea = "";
			String zona;
			int cantidad;
			String[] ciudad;

			linea = br.readLine();

			while (linea != null) {

				zona = linea;
				linea = br.readLine();
				cantidad = Integer.parseInt(linea);
				linea = br.readLine();
				ciudad = new String[cantidad];

				for (int i = 0; i < cantidad; i++) {
					ciudad[i] = String.valueOf(linea);
					linea = br.readLine();
				}

				Itinerario iti = new Itinerario(zona, cantidad, ciudad);

				tablaIti[posicion] = iti;
				posicion++;

			}
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void finalFichero() {
		try {

			br = new BufferedReader(fr);

			String linea = "";

			if ((linea = br.readLine()) == null) {

				System.out.println("lectura finalizada " + linea);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void vaciar(File archivo) {
		if (archivo.delete()) {
			try {
				archivo.createNewFile();
			} catch (IOException e) {

				System.out.println(e.getMessage());
			}
		}

	}

	public void cerrar(Apertura tipo) {
		try {

			if (tipo == Apertura.LEER) {
				tipoA = Apertura.LEER;
				br.close();

			}

			else if (tipo == Apertura.ESCRIBIR) {
				tipoA = Apertura.ESCRIBIR;
				bw.close();

			}

		} catch (IOException e) {
			System.out.println("El fichero no se encuentra abierto");
		}
	}

	public void borrar(Scanner sc) throws IOException {
		//abrir(fichero, Apertura.ESCRIBIR);
		//leer();

		int borrar;

		for (int i = 0; i < tablaIti.length; i++) {
			if (tablaIti[i] != null) {
				System.out.println("[" + i + "]" + tablaIti[i]);
			}
		}
		System.out.println("Indique Itinerario a borrar: ");
		borrar = sc.nextInt();

		tablaIti[borrar] = null; // De esta manera ya estaria borrada la tabla de la estructura

		try {

			bw = new BufferedWriter(fw); // Aqui volvemos a escribir los itinerarios sin el itinerario borrado en el
											// fichero

			for (int i = 0; i < tablaIti.length; i++) {
				if (tablaIti[i] != null) {
					bw.write(tablaIti[i].toString());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
