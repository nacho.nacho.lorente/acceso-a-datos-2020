import java.util.Scanner;

public class Main {

	static final Scanner entrada = new Scanner(System.in);

	public static int escribirMenu() {
		

		//System.out.println("              Menu               ");
		System.out.println("---------------------------------");
		System.out.println("|1. Leer de consola un n�mero   |");
		System.out.println("|2. Escribir n�mero por pantalla|");
		System.out.println("|3. Escribir el n�mero m�s alto |");
		System.out.println("|4. Escribir el n�mero m�s bajo |");
		System.out.println("|5. Borrar n�mero               |");
		System.out.println("|6. Salir                       |");
		System.out.println("---------------------------------");

		return entrada.nextInt();
	}

	public static void main(String[] args) {
		TablaNumeros tablanumeros = new TablaNumeros();
		int opcion = 0;

		do {
			try {

				opcion = escribirMenu();
				switch (opcion) {
				case 1:
					tablanumeros.escribirNumero();
					break;
				case 2:
					System.out.println(tablanumeros.toString());
					break;
				case 3:
					System.out.println(tablanumeros.mayorNumero());
					break;
				case 4:
					System.out.println(tablanumeros.menorNumero());
					break;
				case 5:
					tablanumeros.borrarNumero();
					break;
				}
			} catch (Exception e) {
				System.out.println("No es valido");

			}

		} while (opcion != 6);

	}

}
