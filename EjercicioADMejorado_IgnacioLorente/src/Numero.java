
public class Numero {

	private Integer valor;

	public Numero(Integer valor) {
		this.valor = valor;
	}

	public Integer getValue() {
		return valor;
	}

	public void setValue(Integer valor) {
		this.valor = valor;
	}
	
	
	public String toString() {
		return "\n\t" + valor + "";
	}


	

}
