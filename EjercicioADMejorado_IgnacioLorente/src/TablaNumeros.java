import java.util.ArrayList;
import java.util.List;

public class TablaNumeros {

	private static List<Numero> tablaNumeros = new ArrayList<Numero>();

	public int getTablaNumeros() {
		return tablaNumeros.size();
	}

	private static int pedirNumero() {
		System.out.println("Escribe numero");
		int num;
		try {
			num = Main.entrada.nextInt();
		} catch (Exception e) {
			Main.entrada.nextLine();
			num = pedirNumero();
		}
		return num;
	}

	public void escribirNumero() {
		int numeroPedido = pedirNumero();
		Numero numero = new Numero(numeroPedido);
		tablaNumeros.add(numero);

	}

	public void borrarNumero() {

		int numeroPedido = pedirNumero();
		Numero numero = null;

		for (int i = 0; i < tablaNumeros.size(); i++) {
			if (numeroPedido == tablaNumeros.get(i).getValue())
				numero = tablaNumeros.get(i);
		}

	}

	public int mayorNumero() {
		int Mayor = Integer.MIN_VALUE;
		for (int i = 0; i < tablaNumeros.size(); i++) {
			if (Mayor < tablaNumeros.get(i).getValue())
				;
			Mayor = tablaNumeros.get(i).getValue();
		}
		return Mayor;
	}

	public int menorNumero() {

		int Menor = Integer.MAX_VALUE;
		for (int i = 0; i < tablaNumeros.size(); i++) {
			if (Menor > tablaNumeros.get(i).getValue())
				Menor = tablaNumeros.get(i).getValue();
		}
		return Menor;
	}

	@Override
	public String toString() {
		return "ListaNumeros " + tablaNumeros.toString();
	}

}
