
public class TablaNumeros {
	
	//Atributos tablaNumeros
	
	private int tabla[]; //tabla de enteros
	private int posicion; //posicion actual tabla
	private int longitud; //tama�o maximo tabla
	
	
	
	public TablaNumeros() {
		tabla = new int[30]; //creacion tabla con 30 enteros
		longitud = 30;
		posicion = 0;
		
	}

	public void insertarNumero(int numero) {
		tabla[posicion] = numero;
		posicion++;
		
	}
	
	public void borrarNumero() {
		tabla[posicion] = -1;
		posicion--;
	}
	
	public String imprimir() {
		String cadena = "";
		for(int i =0; i<posicion; i++) {
			cadena +=" [" + i + "] = " + tabla[i] + "\n";
		}
			return cadena;			
	}
	
	public int mayor() {
		int maximo = -10000;
		for(int i=0; i<posicion; i++) {
			if(tabla[i]>maximo)
				maximo = tabla[i];
		}
		return maximo;

	}
	
	public int menor() {
		int minimo = 10000;
		for(int i=0; i<posicion; i++) {
			if(tabla[i]>minimo)
				minimo = tabla[i];
		}
		return minimo;

	}
	
}
