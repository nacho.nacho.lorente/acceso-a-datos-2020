import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	
	static final Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String nombre;
		
		System.out.println("Introduce el nombre a buscar: ");
		nombre = sc.next();
		
		buscar(nombre);

		
	}
	
	public static void buscar(String nombre) {
		try {
		File fichero = new File ("src\\Nombres.txt");
		FileReader fr = new FileReader(fichero);
		BufferedReader br = new BufferedReader(fr);
		int posicion;
		String linea;
		
		
		linea = br.readLine();
		posicion = 1;
		
			while(linea != null) {
				if(nombre.equals(linea)) {
					System.out.println("El nombre aparece en la posicion "+ posicion +" del fichero");
				}else {
					posicion++;
					linea = br.readLine();
				}
			}
			if(linea==null) {
				System.out.println("No se ha encontrado el nombre en el fichero");
				
					br.close();
					
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		
		
	}
}


