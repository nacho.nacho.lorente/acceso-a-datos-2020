import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Metodos {

	public Categoria[] tCat;
	public Producto[] tProd;
	public int posicion;
	File ficheroC = new File("FicheroCategoria.txt");
	File ficheroP = new File("FicheroProducto.txt");
	
	public Metodos(int longitud){
		tCat = new Categoria[longitud];
		tProd = new Producto[longitud];
		posicion = 0;
	}
	
	public void leerCategoria(){
		try {
		FileReader fr = new FileReader(ficheroC);
		BufferedReader br = new BufferedReader(fr);
		
		String linea;
		String idCat, nombre;
		int i = 0;
		
			linea = br.readLine();
			
			while(linea != null) {
				idCat = linea;
				linea = br.readLine();
				nombre = linea;
				tCat[i] = new Categoria(idCat, nombre);
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void leerProductos() {
		try {
			
			FileReader fr = new FileReader (ficheroP);
			BufferedReader br = new BufferedReader (fr);
			String idProd, nombre, idCat;
			String linea;
			int i = 0;
			
			linea = br.readLine();
			while(linea != null) {
				idProd = linea;
				linea = br.readLine();
				nombre = linea;
				linea = br.readLine();
				idCat = linea;
				tProd[i] = new Producto(idProd, nombre, idCat);
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void mostrar() {
	String idCat, idProdCat;
	int np;
	
	for(int i = 0; i < tCat.length; i++) {
		idCat = tCat[i].getidCat();
		np = 0;
		for(int j = 0; j < tProd.length; j++) {
			idProdCat = tProd[j].getidCat();
			if(idCat.equals(idProdCat)) {
				np++;
			}
			System.out.println("Categoria "+ tCat[i].getnombre() + ":" + np);
		}
	}
	
	
	
	
	
	
	}
	
	
	
	
	
	
	
	
	
}
