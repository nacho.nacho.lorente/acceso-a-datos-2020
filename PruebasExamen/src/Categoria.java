
public class Categoria {

	String idCat;
	String nombre;
	
	public Categoria(String idCategoria, String nombre) {
		this.idCat = idCategoria;
		this.nombre = nombre;

	}
	
	public String getidCat() {
		return idCat;
	}
	
	public String getnombre() {
		return nombre;
	}

}
